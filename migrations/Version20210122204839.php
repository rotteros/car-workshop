<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210122204839 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE service CHANGE price price VARCHAR(255) NOT NULL, CHANGE vat_rate vat_rate VARCHAR(255) NOT NULL, CHANGE gross_price gross_price VARCHAR(255) NOT NULL, CHANGE vat_amount vat_amount VARCHAR(255) NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE service CHANGE price price NUMERIC(10, 0) NOT NULL, CHANGE vat_rate vat_rate INT NOT NULL, CHANGE gross_price gross_price DOUBLE PRECISION NOT NULL, CHANGE vat_amount vat_amount DOUBLE PRECISION NOT NULL');
    }
}
