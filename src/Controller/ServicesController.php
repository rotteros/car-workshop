<?php

namespace App\Controller;

use App\Entity\Service;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ServicesController extends AbstractController
{
    /**
     * @Route("/services", name="services")
     */
    public function showAllServices()
    {
        $services = $this->getDoctrine()->getRepository(Service::class)->findAll();
        $html = $this->renderView('services/index.html.twig', ['services' => $services]);

        return new Response($html);
    }

    /**
     * @Route("/services/add", name="add_service")
     */
    public function addService(Request $request)
    {
        $service = new Service();

        $form = $this->createFormBuilder()
            ->add('name', TextType::class, array('label' => 'Nazwa usługi:', 'attr' => array('class' => 'form-control', 'placeholder' => 'np. Wymiana sworzni wahacza - przód')))
            ->add('price', TextType::class, array('label' => 'Cena netto [zł]:', 'attr' => array('class' => 'form-control', 'placeholder' => 'np. 100.99')))
            ->add('vatRate', TextType::class, array('label' => 'Stawka VAT [%]:', 'attr' => array('class' => 'form-control', 'placeholder' => 'np. 23')))
            ->add('save', SubmitType::class, array('label' => 'Dodaj', 'attr' => array('class' => 'btn btn-primary btn-lg btn-block')))
            ->getForm();

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid())
        {
            $name = $form['name']->getData();
            $price = $form['price']->getData();
            $vatRate = $form['vatRate']->getData();

            $convertedPrice = number_format($price, 2, '.', '');
            $convertedVatRate = number_format($vatRate, 2, '.', '');
            $vatAmount = ($price * $vatRate) / 100;
            $convertedVatAmount = number_format($vatAmount, 2, '.', '');
            $grossPrice = $price + $vatAmount;
            $convertedGrossPrice = number_format($grossPrice, 2, '.', '');

            $service->setName($name);
            $service->setPrice($convertedPrice);
            $service->setVatRate($convertedVatRate);
            $service->setVatAmount($convertedVatAmount);
            $service->setGrossPrice($convertedGrossPrice);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($service);
            $entityManager->flush();
            $this->addFlash("addService", "Usługa została dodana!");

            return $this->redirectToRoute('services');
        }

        $grossPrice = $service->getGrossPrice();
        $vatAmount = $service->getVatAmount();

        $html = $this->renderView('services/add.html.twig', [
            'form' => $form->createView(),
            'grossPrice' => $grossPrice,
            'vatAmount' => $vatAmount
        ]);

        return new Response($html);
    }

    /**
     * @Route("/services/edit/{id}", name="edit_service")
     */
    public function editService(Request $request, $id)
    {
        $service = $this->getDoctrine()->getRepository(Service::class)->find($id);
        $service->setName($service->getName());
        $service->setPrice($service->getPrice());
        $service->setVatRate($service->getVatRate());

        $form = $this->createFormBuilder($service)
            ->add('name', TextType::class, array('label' => 'Nazwa usługi:', 'attr' => array('class' => 'form-control')))
            ->add('price', TextType::class, array('label' => 'Cena netto [zł]:', 'attr' => array('class' => 'form-control')))
            ->add('vatRate', TextType::class, array('label' => 'Stawka VAT [%]:', 'attr' => array('class' => 'form-control')))
            ->add('save', SubmitType::class, array('label' => 'Edytuj', 'attr' => array('class' => 'btn btn-primary btn-lg btn-block')))
            ->getForm();

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid())
        {
            $name = $form['name']->getData();
            $price = $form['price']->getData();
            $vatRate = $form['vatRate']->getData();

            $convertedPrice = number_format($price, 2, '.', '');
            $convertedVatRate = number_format($vatRate, 2, '.', '');
            $vatAmount = ($price * $vatRate) / 100;
            $convertedVatAmount = number_format($vatAmount, 2, '.', '');
            $grossPrice = $price + $vatAmount;
            $convertedGrossPrice = number_format($grossPrice, 2, '.', '');

            $entityManager = $this->getDoctrine()->getManager();
            $service = $entityManager->getRepository(Service::class)->find($id);

            $service->setName($name);
            $service->setPrice($convertedPrice);
            $service->setVatRate($convertedVatRate);
            $service->setVatAmount($convertedVatAmount);
            $service->setGrossPrice($convertedGrossPrice);
            $entityManager->flush();
            $this->addFlash("editService", "Edycja danych usługi została wykonana pomyślnie!");

            return $this->redirectToRoute('services');
        }

        $html = $this->renderView('services/edit.html.twig', [
            'form' => $form->createView()
        ]);

        return new Response($html);
    }

    /**
     * @Route("/services/delete/{id}", name="delete_service")
     */
    public function deleteService($id)
    {
        try
        {
            $entityManager = $this->getDoctrine()->getManager();
            $service = $entityManager->getRepository(Service::class)->find($id);
            $entityManager->remove($service);
            $entityManager->flush();
            $this->addFlash("deleteService", "Usługa została usunięta!");

            return $this->redirectToRoute('services');
        } catch (\Exception $exception) {
            $this->addFlash("conflictDeleteService", "Nie możesz usunąć usługi, która jest przypisana do zlecenia!");

            return $this->redirectToRoute('services');
        }
    }
}
