<?php

namespace App\Controller;

use App\Entity\Car;
use App\Entity\Client;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class CarsController extends AbstractController
{

    /**
     * @Route("/cars", name="cars")
     */
    public function showAllCars()
    {
        $cars = $this->getDoctrine()->getRepository(Car::class)->findAll();
        $html = $this->renderView('cars/index.html.twig', ['cars' => $cars]);

        return new Response($html);
    }

    /**
     * @Route("/cars/add", name="add_car")
     */
    public function addCar(Request $request)
    {
        $car = new Car();

        $form = $this->createFormBuilder()
            ->add('id', EntityType::class, [
                'class' => Client::class,
                'label' => 'Wybierz klienta:',
                'attr' => array('class' => 'form-control chosen'),
                'choice_label' => function (Client $clientEntity) {
                    return vsprintf('%s', [
                        $clientEntity->getName(),
                    ]);
                },
            ])
            ->add('brand', TextType::class, array('label' => 'Marka:', 'attr' => array('class' => 'form-control', 'placeholder' => 'np. Audi')))
            ->add('model', TextType::class, array('label' => 'Model:', 'attr' => array('class' => 'form-control', 'placeholder' => 'np. A4')))
            ->add('production_date', TextType::class, array('label' => 'Rok produkcji:', 'attr' => array('class' => 'form-control', 'placeholder' => 'np. 2005')))
            ->add('capacity', TextType::class, array('label' => 'Pojemność [cm3]:', 'attr' => array('class' => 'form-control', 'placeholder' => 'np. 1900')))
            ->add('mileage', TextType::class, array('label' => 'Przebieg [km]:', 'attr' => array('class' => 'form-control', 'placeholder' => 'np. 275320')))
            ->add('vin', TextType::class, array('label' => 'Numer VIN:', 'attr' => array('class' => 'form-control', 'placeholder' => 'np. WAUZZZ8E85A500326')))
            ->add('registration_number', TextType::class, array('label' => 'Numer rejestracyjny:', 'attr' => array('class' => 'form-control', 'placeholder' => 'np. ZBI R095')))
            ->add('save', SubmitType::class, array('label' => 'Dodaj', 'attr' => array('class' => 'btn btn-primary btn-lg btn-block')))
            ->getForm();

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid())
        {
            $client_id = $form['id']->getData();
            $brand = $form['brand']->getData();
            $model = $form['model']->getData();
            $productionDate = $form['production_date']->getData();
            $capacity = $form['capacity']->getData();
            $mileage = $form['mileage']->getData();
            $vin = $form['vin']->getData();
            $registrationNumber = $form['registration_number']->getData();

            $car->setClient($client_id);
            $car->setBrand($brand);
            $car->setModel($model);
            $car->setProductionDate($productionDate);
            $car->setCapacity($capacity);
            $car->setMileage($mileage);
            $car->setVin($vin);
            $car->setRegistrationNumber($registrationNumber);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($car);
            $entityManager->flush();
            $this->addFlash("addCar", "Samochód został dodany!");

            return $this->redirectToRoute('cars');
        }

        $html = $this->renderView('cars/add.html.twig', [
            'form' => $form->createView()
        ]);

        return new Response($html);
    }

    /**
     * @Route("/cars/view/{id}", name="view_car")
     */
    public function viewCar($id)
    {
        $car = $this->getDoctrine()->getRepository(Car::class)->find($id);
        $html = $this->renderView('cars/view.html.twig', ['car' => $car]);

        return new Response($html);
    }

    /**
     * @Route("/cars/edit/{id}", name="edit_car")
     */
    public function editCar(Request $request, $id)
    {
        $car = $this->getDoctrine()->getRepository(Car::class)->find($id);
        $car->setBrand($car->getBrand());
        $car->setModel($car->getModel());
        $car->setProductionDate($car->getProductionDate());
        $car->setCapacity($car->getCapacity());
        $car->setMileage($car->getMileage());
        $car->setVin($car->getVin());
        $car->setRegistrationNumber($car->getRegistrationNumber());

        $form = $this->createFormBuilder($car)
            ->add('brand', TextType::class, array('label' => 'Marka:', 'attr' => array('class' => 'form-control')))
            ->add('model', TextType::class, array('label' => 'Model:', 'attr' => array('class' => 'form-control')))
            ->add('production_date', TextType::class, array('label' => 'Rok produkcji:', 'attr' => array('class' => 'form-control')))
            ->add('capacity', TextType::class, array('label' => 'Pojemność [cm3]:', 'attr' => array('class' => 'form-control')))
            ->add('mileage', TextType::class, array('label' => 'Przebieg [km]:', 'attr' => array('class' => 'form-control')))
            ->add('vin', TextType::class, array('label' => 'Numer VIN:', 'attr' => array('class' => 'form-control')))
            ->add('registration_number', TextType::class, array('label' => 'Numer rejestracyjny:', 'attr' => array('class' => 'form-control')))
            ->add('save', SubmitType::class, array('label' => 'Edytuj', 'attr' => array('class' => 'btn btn-primary btn-lg btn-block')))
            ->getForm();

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid())
        {
            $brand = $form['brand']->getData();
            $model = $form['model']->getData();
            $productionDate = $form['production_date']->getData();
            $capacity = $form['capacity']->getData();
            $mileage = $form['mileage']->getData();
            $vin = $form['vin']->getData();
            $registrationNumber = $form['registration_number']->getData();

            $entityManager = $this->getDoctrine()->getManager();
            $car = $entityManager->getRepository(Car::class)->find($id);

            $car->setBrand($brand);
            $car->setModel($model);
            $car->setProductionDate($productionDate);
            $car->setCapacity($capacity);
            $car->setMileage($mileage);
            $car->setVin($vin);
            $car->setRegistrationNumber($registrationNumber);
            $entityManager->flush();
            $this->addFlash("editCar", "Edycja danych samochodu została wykonana pomyślnie!");

            return $this->redirectToRoute('cars');
        }

        $html = $this->renderView('cars/edit.html.twig', [
            'form' => $form->createView()
        ]);

        return new Response($html);
    }

    /**
     * @Route("/cars/delete/{id}", name="delete_car")
     */
    public function deleteCar($id)
    {
        try
        {
            $entityManager = $this->getDoctrine()->getManager();
            $car = $entityManager->getRepository(Car::class)->find($id);
            $entityManager->remove($car);
            $entityManager->flush();
            $this->addFlash("deleteCar", "Samochód został usunięty!");

            return $this->redirectToRoute('cars');
        } catch (\Exception $exception) {
            $this->addFlash("conflictDeleteCar", "Nie możesz usunąć samochodu, który posiada przypisane zlecenie!");

            return $this->redirectToRoute('cars');
        }
    }
}


