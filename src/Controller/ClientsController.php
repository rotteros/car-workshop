<?php

namespace App\Controller;

use App\Entity\Client;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ClientsController extends AbstractController
{

    /**
     * @Route("/clients", name="clients")
     */
    public function showAllClients()
    {
        $clients = $this->getDoctrine()->getRepository(Client::class)->findAll();
        $html = $this->renderView('clients/index.html.twig', ['clients' => $clients]);

        return new Response($html);
    }

    /**
     * @Route("/clients/add", name="add_client")
     */
    public function addClient(Request $request)
    {
        $client = new Client();

        $form = $this->createFormBuilder($client)
            -> add('name', TextType::class, array('label' => 'Imię i nazwisko:', 'attr' => array('class' => 'form-control', 'placeholder' => 'np. Jan Kowalski')))
            -> add('email', EmailType::class, array('label' => 'Adres e-mail:', 'attr' => array('class' => 'form-control', 'placeholder' => 'np. jan.kowalski@wp.pl')))
            -> add('street', TextType::class, array('label' => 'Ulica:', 'attr' => array('class' => 'form-control', 'placeholder' => 'np. Koniecpolskiego 7/10')))
            -> add('postalCode', TextType::class, array('label' => 'Kod pocztowy:', 'attr' => array('class' => 'form-control', 'placeholder' => 'np. 78-100')))
            -> add('city', TextType::class, array('label' => 'Miasto:', 'attr' => array('class' => 'form-control', 'placeholder' => 'np. Kołobrzeg')))
            -> add('telephone', TextType::class, array('label' => 'Numer telefonu:','attr' => array('class' => 'form-control', 'placeholder' => 'np. 503-253-163')))
            -> add('nip', TextType::class, array('label' => 'Numer NIP:','attr' => array('class' => 'form-control', 'placeholder' => 'np. 132-22-63-249')))
            -> add('save', SubmitType::class, array('label' => 'Dodaj', 'attr' => array('class' => 'btn btn-primary btn-lg btn-block')))
            -> getForm();

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid())
        {
            $name = $form['name']->getData();
            $email = $form['email']->getData();
            $street = $form['street']->getData();
            $postalCode = $form['postalCode']->getData();
            $city = $form['city']->getData();
            $telephone = $form['telephone']->getData();
            $nip = $form['nip']->getData();

            $client->setName($name);
            $client->setEmail($email);
            $client->setStreet($street);
            $client->setPostalCode($postalCode);
            $client->setCity($city);
            $client->setTelephone($telephone);
            $client->setNip($nip);

            $em = $this->getDoctrine()->getManager();
            $em->persist($client);
            $em->flush();
            $this->addFlash("addClient", "Klient został dodany!");

            return $this->redirectToRoute('clients');
        }

        $html = $this->renderView('clients/add.html.twig', [
            'form' => $form->createView()
        ]);

        return new Response($html);
    }

    /**
     * @Route("/clients/view/{id}", name="view_client")
     */
    public function viewClient($id)
    {
        $client = $this->getDoctrine()->getRepository(Client::class)->find($id);
        $html = $this->renderView('clients/view.html.twig' , ['client' => $client]);

        return new Response($html);
    }

    /**
     * @Route("/clients/edit/{id}", name="edit_client")
     */
    public function editClient(Request $request, $id)
    {
        $client = $this->getDoctrine()->getRepository(Client::class)->find($id);
        $client->setName($client->getName());
        $client->setEmail($client->getEmail());
        $client->setStreet($client->getStreet());
        $client->setPostalCode($client->getPostalCode());
        $client->setCity($client->getCity());
        $client->setTelephone($client->getTelephone());
        $client->setNip($client->getNip());

        $form = $this->createFormBuilder($client)
            -> add('name', TextType::class, array('label' => 'Imię i nazwisko:', 'attr' => array('class' => 'form-control')))
            -> add('email', EmailType::class, array('label' => 'Adres e-mail:', 'attr' => array('class' => 'form-control')))
            -> add('street', TextType::class, array('label' => 'Ulica:', 'attr' => array('class' => 'form-control')))
            -> add('postalCode', TextType::class, array('label' => 'Kod pocztowy:', 'attr' => array('class' => 'form-control')))
            -> add('city', TextType::class, array('label' => 'Miasto:', 'attr' => array('class' => 'form-control')))
            -> add('telephone', TextType::class, array('label' => 'Numer telefonu:','attr' => array('class' => 'form-control')))
            -> add('nip', TextType::class, array('label' => 'Numer NIP:','attr' => array('class' => 'form-control')))
            -> add('save', SubmitType::class, array('label' => 'Edytuj', 'attr' => array('class' => 'btn btn-primary btn-lg btn-block')))
            -> getForm();

        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid())
        {
            $name = $form['name']->getData();
            $email = $form['email']->getData();
            $street = $form['street']->getData();
            $postalCode = $form['postalCode']->getData();
            $city = $form['city']->getData();
            $telephone = $form['telephone']->getData();
            $nip = $form['nip']->getData();

            $entityManager = $this->getDoctrine()->getManager();
            $client = $entityManager->getRepository(Client::class)->find($id);

            $client->setName($name);
            $client->setEmail($email);
            $client->setStreet($street);
            $client->setPostalCode($postalCode);
            $client->setCity($city);
            $client->setTelephone($telephone);
            $client->setNip($nip);
            $entityManager->flush();
            $this->addFlash("editClient", "Edycja danych klienta została wykonana pomyślnie!");

            return $this->redirectToRoute('clients');
        }

        $html = $this->renderView('clients/edit.html.twig', [
            'form' => $form->createView()
        ]);

        return new Response($html);
    }

    /**
     * @Route("/clients/delete/{id}", name="delete_client")
     */
    public function deleteClient($id)
    {
        try
        {
            $entityManager = $this->getDoctrine()->getManager();
            $client = $entityManager->getRepository(Client::class)->find($id);

            $entityManager->remove($client);
            $entityManager->flush();
            $this->addFlash("deleteClient", "Klient został usunięty!");
        } catch(\Exception $exception) {
            $this->addFlash("deleteClient", "Nie możesz usunąć klienta, który ma przypisany samochód!");
        }

        return $this->redirectToRoute('clients');
    }
}
