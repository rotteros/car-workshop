<?php

namespace App\Controller;

use App\Entity\Invoice;
use App\Entity\MadeService;
use App\Entity\Order;
use Dompdf\Dompdf;
use Dompdf\Options;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class InvoicesController extends AbstractController
{

    /**
     * @Route("/invoices", name="invoices")
     */
    public function showAllInvoices()
    {
        $invoices = $this->getDoctrine()->getRepository(Invoice::class)->findAll();
        $html = $this->renderView('invoices/index.html.twig', ['invoices' => $invoices]);

        return new Response($html);
    }

    /**
     * @Route("/invoices/add", name="add_invoice")
     */
    public function addInvoice(Request $request)
    {
        $invoice = new Invoice();

        $form = $this->createFormBuilder()
            ->add('id', EntityType::class, [
                'class' => Order::class,
                'label' => 'Wybierz numer rejestracyjny auta:',
                'attr' => array('class' => 'form-control chosen'),
                'choice_label' => function (Order $orderEntity) {
                    return vsprintf('%s [Zlecenie numer: %s]', [
                        $orderEntity->getCar()->getRegistrationNumber(),
                        $orderEntity->getId(),
                    ]);
                },
            ])
            ->add('date', TextType::class, array('label' => 'Data wystawienia:', 'attr' => array('class' => 'form-control', 'placeholder' => 'np. 22.01.2021')))
            ->add('save', SubmitType::class, array('label' => 'Wystaw', 'attr' => array('class' => 'btn btn-primary btn-lg btn-block')))
            ->getForm();

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid())
        {
            $orderId = $form['id']->getData();
            $date = $form['date']->getData();

            $invoice->setOrders($orderId);
            $invoice->setDate($date);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($invoice);
            $entityManager->flush();
            $this->addFlash("addInvoice", "Faktura została wystawiona!");

            return $this->redirectToRoute('invoices');
        }

        $html = $this->renderView('invoices/add.html.twig', [
            'form' => $form->createView(),
        ]);

        return new Response($html);
    }

    /**
     * @Route("/invoices/edit/{id}", name="edit_invoice")
     */
    public function editInvoice(Request $request, $id)
    {
        $invoice = $this->getDoctrine()->getRepository(Invoice::class)->find($id);
        $invoice->setDate($invoice->getDate());

        $form = $this->createFormBuilder($invoice)
            ->add('date', TextType::class, array('label' => 'Data wystawienia:', 'attr' => array('class' => 'form-control')))
            ->add('save', SubmitType::class, array('label' => 'Edytuj', 'attr' => array('class' => 'btn btn-primary btn-lg btn-block')))
            ->getForm();

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid())
        {
            $date = $form['date']->getData();

            $entityManager = $this->getDoctrine()->getManager();
            $invoice = $entityManager->getRepository(Invoice::class)->find($id);
            $invoice->setDate($date);
            $entityManager->flush();
            $this->addFlash("editInvoice", "Edycja danych faktury została wykonana pomyślnie!");

            return $this->redirectToRoute('invoices');
        }

        $html = $this->renderView('invoices/edit.html.twig', [
            'form' => $form->createView()
        ]);

        return new Response($html);
    }

    /**
     * @Route("/invoices/delete/{id}", name="delete_invoice")
     */
    public function deleteInvoice($id)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $invoice = $entityManager->getRepository(Invoice::class)->find($id);
        $entityManager->remove($invoice);
        $entityManager->flush();
        $this->addFlash("deleteInvoice", "Faktura została usunięta!");

        return $this->redirectToRoute('invoices');
    }

    /**
     * @Route("/invoices/print/{id}", name="print_invoice")
     */
    public function printInvoice($id)
    {
        $pdfOptions = new Options();
        $pdfOptions->set('defaultFont', 'Arial');

        $dompdf = new Dompdf($pdfOptions);

        $invoice = $this->getDoctrine()->getRepository(Invoice::class)->find($id);
        $orderId = $invoice->getOrders()->getId();

        $madeServicesByOrder = $this->getDoctrine()->getRepository(MadeService::class)->findBy( array('orders' => $orderId));

        $html = $this->renderView('invoices/print.html.twig', [
            'invoice' => $invoice,
            'madeServicesByOrder' => $madeServicesByOrder,
        ]);

        $dompdf->loadHtml($html);
        $dompdf->setPaper('A4', 'portrait');
        $dompdf->render();
        $dompdf->stream("faktura.pdf", [
            "Attachment" => false
        ]);

        return new Response($html);
    }
}
