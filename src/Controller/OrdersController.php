<?php

namespace App\Controller;

use App\Entity\Car;
use App\Entity\MadeService;
use App\Entity\Order;
use App\Entity\Service;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class OrdersController extends AbstractController
{

    /**
     * @Route("/orders", name="orders")
     */
    public function showAllOrders()
    {
        $orders = $this->getDoctrine()->getRepository(Order::class)->findAll();
        $html = $this->renderView('orders/index.html.twig', ['orders' => $orders]);

        return new Response($html);
    }

    /**
     * @Route("/orders/view/{id}", name="view_order")
     */
    public function viewOrder(Request $request, $id)
    {
        $order = $this->getDoctrine()->getRepository(Order::class)->find($id);
        $order->setStatus($order->getStatus());

        $form = $this->createFormBuilder($order)
            ->add('status', ChoiceType::class, [
                'label' => false,
                'attr' => ['class' => 'form-control chosen'],
                'choices' => [
                        'Oczekujące' => '1',
                        'Diagnozowanie' => '2',
                        'Naprawa' => '3',
                        'Zakończone' => '4',
                    ]])
            ->add('save', SubmitType::class, array('label' => 'Zmień', 'attr' => array('class' => 'btn btn-primary btn-lg btn-block')))
            ->getForm();

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid())
        {
            $status = $form['status']->getData();

            $entityManager = $this->getDoctrine()->getManager();
            $order = $entityManager->getRepository(Order::class)->find($id);
            $order->setStatus($status);
            $entityManager->flush();
            $this->addFlash("changeOrderStatus", "Status zlecenia został zmieniony!");

            return $this->redirectToRoute('orders');
        }

        $html = $this->renderView('orders/view.html.twig', [
            'order' => $order,
            'form' => $form->createView()
        ]);

        return new Response($html);
    }

    /**
     * @Route("/orders/add", name="add_order")
     */
    public function addOrder(Request $request)
    {
        $order = new Order();

        $form = $this->createFormBuilder()
            ->add('id', EntityType::class, [
                'class' => Car::class,
                'label' => 'Wybierz numer rejestracyjny pojazdu:',
                'attr' => array('class' => 'form-control chosen'),
                'choice_label' => function (Car $carEntity) {
                    return vsprintf('%s', [
                        $carEntity->getRegistrationNumber(),
                    ]);
                },
            ])
            ->add('comments', TextareaType::class, array('label' => 'Uwagi klienta:', 'attr' => array('class' => 'form-control', 'cols' => '5', 'rows' => '5', 'placeholder' => 'np. Samochód nie odpala.')))
            ->add('save', SubmitType::class, array('label' => 'Dodaj', 'attr' => array('class' => 'btn btn-primary btn-lg btn-block')))
            ->getForm();

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid())
        {
            $car_id = $form['id']->getData();
            $comments = $form['comments']->getData();

            $order->setCar($car_id);
            $order->setComments($comments);
            $order->setStatus(1);
            $order->setIsArchive(0);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($order);
            $entityManager->flush();
            $this->addFlash("addOrder", "Zlecenie zostało dodane!");

            return $this->redirectToRoute('orders');
        }

        $html = $this->renderView('orders/add.html.twig', [
            'form' => $form->createView()
        ]);

        return new Response($html);
    }

    /**
     * @Route("/orders/edit/{id}", name="edit_order")
     */
    public function editOrder(Request $request, $id)
    {
        $order = $this->getDoctrine()->getRepository(Order::class)->find($id);
        $order->setComments($order->getComments());

        $form = $this->createFormBuilder($order)
            ->add('comments', TextareaType::class, array('label' => 'Uwagi klienta:', 'attr' => array('class' => 'form-control', 'cols' => '5', 'rows' => '5')))
            ->add('save', SubmitType::class, array('label' => 'Edytuj', 'attr' => array('class' => 'btn btn-primary btn-lg btn-block')))
            ->getForm();

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid())
        {
            $comments = $form['comments']->getData();

            $entityManager = $this->getDoctrine()->getManager();
            $order = $entityManager->getRepository(Order::class)->find($id);
            $order->setComments($comments);
            $entityManager->flush();
            $this->addFlash("editOrder", "Edycja danych zlecenia została wykonana pomyślnie!");

            return $this->redirectToRoute('orders');
        }

        $html = $this->renderView('orders/edit.html.twig', [
            'form' => $form->createView()
        ]);

        return new Response($html);
    }

    /**
     * @Route("/orders/add-service/{id}", name="add_service_to_order")
     */
    public function addServiceToOrder(Request $request, $id)
    {
        $orderId = $this->getDoctrine()->getRepository(Order::class)->find($id);
        $madeService = new MadeService();

        $form = $this->createFormBuilder()
            ->add('id', EntityType::class, [
                'class' => Service::class,
                'label' => false,
                'attr' => array('class' => 'form-control chosen'),
                'choice_label' => function (Service $serviceEntity) {
                    return vsprintf('%s', [
                        $serviceEntity->getName(),
                    ]);
                },
            ])
            ->add('save', SubmitType::class, array('label' => 'Dodaj', 'attr' => array('class' => 'btn btn-primary btn-lg btn-block')))
            ->getForm();

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid())
        {
            $serviceId = $form['id']->getData();

            $madeService->setService($serviceId);
            $madeService->setOrders($orderId);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($madeService);
            $entityManager->flush();
        }

        $madeServicesByOrder = $this->getDoctrine()->getRepository(MadeService::class)->findBy( array('orders' => $id));

        $html = $this->renderView('orders/add-service.html.twig', [
            'madeServicesByOrder' => $madeServicesByOrder,
            'form' => $form->createView()
            ]);

        return new Response($html);
    }

    /**
     * @Route("/orders/delete-service/{id}", name="delete_service_from_order")
     */
    public function deleteServiceFromOrder($id)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $madeService = $entityManager->getRepository(MadeService::class)->find($id);
        $entityManager->remove($madeService);
        $entityManager->flush();
        $this->addFlash("deleteServiceFromOrder", "Wybrana usługa została usunięta ze zlecenia!");

        return $this->redirectToRoute('orders');

    }

    /**
     * @Route("/orders/delete/{id}", name="delete_order")
     */
    public function deleteOrder($id)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $order = $entityManager->getRepository(Order::class)->find($id);
        $order->setisArchive(1);
        $entityManager->flush();
        $this->addFlash("deleteOrder", "Zlecenie zostało usunięte!");

        return $this->redirectToRoute('orders');
    }
}
