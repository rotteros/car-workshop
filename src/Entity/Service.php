<?php

namespace App\Entity;

use App\Repository\ServiceRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ServiceRepository::class)
 */
class Service
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string")
     */
    private $price;

    /**
     * @ORM\OneToMany(targetEntity=MadeService::class, mappedBy="service")
     */
    private $madeServices;

    /**
     * @ORM\Column(type="string")
     */
    private $vatRate;

    /**
     * @ORM\Column(type="string")
     */
    private $grossPrice;

    /**
     * @ORM\Column(type="string")
     */
    private $vatAmount;

    public function __construct()
    {
        $this->madeServices = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPrice(): ?string
    {
        return $this->price;
    }

    public function setPrice(string $price): self
    {
        $this->price = $price;

        return $this;
    }

    /**
     * @return Collection|MadeService[]
     */
    public function getMadeServices(): Collection
    {
        return $this->madeServices;
    }

    public function addMadeService(MadeService $madeService): self
    {
        if (!$this->madeServices->contains($madeService)) {
            $this->madeServices[] = $madeService;
            $madeService->setService($this);
        }

        return $this;
    }

    public function removeMadeService(MadeService $madeService): self
    {
        if ($this->madeServices->removeElement($madeService)) {
            // set the owning side to null (unless already changed)
            if ($madeService->getService() === $this) {
                $madeService->setService(null);
            }
        }

        return $this;
    }

    public function getVatRate(): ?string
    {
        return $this->vatRate;
    }

    public function setVatRate(string $vatRate): self
    {
        $this->vatRate = $vatRate;

        return $this;
    }

    public function getGrossPrice(): ?string
    {
        return $this->grossPrice;
    }

    public function setGrossPrice(string $grossPrice): self
    {
        $this->grossPrice = $grossPrice;

        return $this;
    }

    public function getVatAmount(): ?string
    {
        return $this->vatAmount;
    }

    public function setVatAmount(string $vatAmount): self
    {
        $this->vatAmount = $vatAmount;

        return $this;
    }
}
